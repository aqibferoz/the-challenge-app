import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RematchPage } from '../rematch/rematch';

/**
 * Generated class for the ChallengeFinishedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-challenge-finished',
  templateUrl: 'challenge-finished.html',
})
export class ChallengeFinishedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChallengeFinishedPage');
  }
  rematch(){
    this.navCtrl.push(RematchPage)
  }
}
