import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChallengeFinishedPage } from './challenge-finished';

@NgModule({
  declarations: [
    ChallengeFinishedPage,
  ],
  imports: [
    IonicPageModule.forChild(ChallengeFinishedPage),
  ],
})
export class ChallengeFinishedPageModule {}
