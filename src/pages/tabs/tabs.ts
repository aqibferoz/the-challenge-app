import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;  //challenges
  tab2Root = AboutPage;  //profile
  tab3Root = ContactPage; //shop

  constructor() {

  }
}
