import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultNotClearPage } from './result-not-clear';

@NgModule({
  declarations: [
    ResultNotClearPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultNotClearPage),
  ],
})
export class ResultNotClearPageModule {}
