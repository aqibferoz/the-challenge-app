import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LastMatchesPage } from './last-matches';

@NgModule({
  declarations: [
    LastMatchesPage,
  ],
  imports: [
    IonicPageModule.forChild(LastMatchesPage),
  ],
})
export class LastMatchesPageModule {}
