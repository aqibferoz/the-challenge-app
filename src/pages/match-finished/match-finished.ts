import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChallengeFinishedPage } from '../challenge-finished/challenge-finished';

/**
 * Generated class for the MatchFinishedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-match-finished',
  templateUrl: 'match-finished.html',
})
export class MatchFinishedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MatchFinishedPage');
  }
  finish(){
    this.navCtrl.push(ChallengeFinishedPage)
  }
}
