import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchFinishedPage } from './match-finished';

@NgModule({
  declarations: [
    MatchFinishedPage,
  ],
  imports: [
    IonicPageModule.forChild(MatchFinishedPage),
  ],
})
export class MatchFinishedPageModule {}
