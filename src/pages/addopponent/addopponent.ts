import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StartgamePage } from '../startgame/startgame';

/**
 * Generated class for the AddopponentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addopponent',
  templateUrl: 'addopponent.html',
})
export class AddopponentPage {
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddopponentPage');
  
  }
  startGame(){
    this.navCtrl.push(StartgamePage)
  }
}
