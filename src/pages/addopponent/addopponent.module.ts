import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddopponentPage } from './addopponent';

@NgModule({
  declarations: [
    AddopponentPage,
  ],
  imports: [
    IonicPageModule.forChild(AddopponentPage),
  ],
})
export class AddopponentPageModule {}
