import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GamepicturePage } from '../gamepicture/gamepicture';

/**
 * Generated class for the StartgamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-startgame',
  templateUrl: 'startgame.html',
})
export class StartgamePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartgamePage');
  }
  takePhoto(){
    this.navCtrl.push(GamepicturePage)
  }
}
