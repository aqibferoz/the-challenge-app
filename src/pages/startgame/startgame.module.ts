import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartgamePage } from './startgame';

@NgModule({
  declarations: [
    StartgamePage,
  ],
  imports: [
    IonicPageModule.forChild(StartgamePage),
  ],
})
export class StartgamePageModule {}
