import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChosenCategoryPage } from '../chosen-category/chosen-category';

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {
data = [
  {
    object : "Free points"
  },
  {
    object : "Buy points"
  },
  {
    object : "Free tickets"
  },
  {
    object : "Tickets"
  },
  {
    object : "Free points"
  }
]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPage');
  }
  chosenCategory(){
    this.navCtrl.push(ChosenCategoryPage)
  }
}
