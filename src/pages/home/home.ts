import { Component, Input, ViewChild, AfterViewInit } from '@angular/core';
import { NavController, Content, FabButton } from 'ionic-angular';
import { ChallengePage } from '../challenge/challenge';
import { ApiProvider } from '../../providers/api/api';
import { CreateChallengePage } from '../create-challenge/create-challenge';
import { first, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements AfterViewInit{
    @Input() events: any;
    @ViewChild(Content)
    content: Content;
    @ViewChild(FabButton)
    fabButton: FabButton;

    private challenges: any;

    constructor(public navCtrl: NavController, private api: ApiProvider) {

        const challenges = this.api.getChallenges().pipe(first(), map(
            list => {
                return list.map(
                    challenges => {
                        return { id: challenges.payload.doc.id, ...challenges.payload.doc.data() };
                    }
                )
            }
        )).toPromise();

        challenges.then(c => {
            this.challenges = c;
        });

    }

    faButton() {
        this.navCtrl.push(CreateChallengePage)
    }
    ngAfterViewInit() {
        this.content.ionScroll.subscribe((d) => {
            this.fabButton.setElementClass("fab-button-out", d.directionY == "down");
        });
    }
    openPage(item) {
        let i = item;
        this.navCtrl.push(ChallengePage, { i })
    }
}
