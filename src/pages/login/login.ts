import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

//import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { TabsPage } from '../tabs/tabs';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  data = {
    "logo": "assets/images/logo/login.png",
    "errorUser": "Field can't be empty.",
    "errorPassword": "Field can't be empty."
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController,
    private auth: AuthProvider, private api: ApiProvider, private helper: HelperProvider) {

  };

  @Input() events: any;

  public email: string;
  public password: string;

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {

    this.helper.load('Signing in the user please wait');
    
    if (this.email != null && this.password != null) {
      this.auth.login(this.email, this.password)
      .then(
        user => {
          this.helper.dismiss();
          this.auth.saveToken(user.user.uid);
          this.navCtrl.push(TabsPage);
        }
      ).catch(err => {
        this.helper.dismiss();
        this.helper.presentBottomToast(err);
        console.log(err);
      });
    } else {

      this.helper.presentBottomToast('Either email or password is empty...');
      this.helper.dismiss();
    }
  
  }

  register() {
    this.navCtrl.push(RegisterPage)
  }
forgotPassword(){
  const prompt = this.alertCtrl.create({
    title: 'Reset password',
    
    inputs: [
      {
        name: 'email',
        placeholder: 'Email address'
      },
    ],
    buttons: [
      {
        text: 'Send email',
        handler: data => {
          console.log('send clicked');
        }
      },
     
    ]
  });
  prompt.present();
}
}
