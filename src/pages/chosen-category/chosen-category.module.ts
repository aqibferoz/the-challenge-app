import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChosenCategoryPage } from './chosen-category';

@NgModule({
  declarations: [
    ChosenCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ChosenCategoryPage),
  ],
})
export class ChosenCategoryPageModule {}
