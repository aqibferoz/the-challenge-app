import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ChosenCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chosen-category',
  templateUrl: 'chosen-category.html',
})
export class ChosenCategoryPage {
  data = [
    {
    image : 'assets/images/background/6.jpg',
    price : "1000",
  },
  {
    image : 'assets/images/background/7.jpg',
    price : "2000",
  },
  {
    image : 'assets/images/background/8.jpg',
    price : "56545",
  },
  {
    image : 'assets/images/background/2.jpg',
    price : "8000",
  },
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChosenCategoryPage');
  }

}
