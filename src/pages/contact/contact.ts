import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CategoriesPage } from '../categories/categories';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
data = [
  {
  image : 'assets/images/background/6.jpg',
  price : "1000",
},
{
  image : 'assets/images/background/7.jpg',
  price : "2000",
},
{
  image : 'assets/images/background/8.jpg',
  price : "56545",
},
{
  image : 'assets/images/background/2.jpg',
  price : "8000",
},
]
  constructor(public navCtrl: NavController) {
    
  }
  categories(){
    this.navCtrl.push(CategoriesPage)
  }
}
