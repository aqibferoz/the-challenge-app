import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MatchOngoingPage } from '../match-ongoing/match-ongoing';

/**
 * Generated class for the GamepicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-gamepicture',
  templateUrl: 'gamepicture.html',
})
export class GamepicturePage {
  data = 
    {
   
    image:"assets/images/background/0.jpg",
    
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GamepicturePage');
  }
beginMatch(){
  this.navCtrl.push(MatchOngoingPage)
}
}
