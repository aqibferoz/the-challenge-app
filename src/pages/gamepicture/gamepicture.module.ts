import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GamepicturePage } from './gamepicture';

@NgModule({
  declarations: [
    GamepicturePage,
  ],
  imports: [
    IonicPageModule.forChild(GamepicturePage),
  ],
})
export class GamepicturePageModule {}
