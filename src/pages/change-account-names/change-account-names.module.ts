import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeAccountNamesPage } from './change-account-names';

@NgModule({
  declarations: [
    ChangeAccountNamesPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeAccountNamesPage),
  ],
})
export class ChangeAccountNamesPageModule {}
