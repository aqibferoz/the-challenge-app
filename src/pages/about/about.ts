import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AccountPage } from '../account/account';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
data = {
  image : 'assets/images/avatar/2.jpg',
  balance : "1000",
  points : "139",
  title: "Aqib Feroz",
  rank : "47"
}
  constructor(public navCtrl: NavController) {

  }
  account(){
    this.navCtrl.push(AccountPage)
  }
}
