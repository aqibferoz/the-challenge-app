import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResultNotClearPage } from '../result-not-clear/result-not-clear';

/**
 * Generated class for the RematchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rematch',
  templateUrl: 'rematch.html',
})
export class RematchPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RematchPage');
  }
  resultNotClear(){
    this.navCtrl.push(ResultNotClearPage)
  }
}
