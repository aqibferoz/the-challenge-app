import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RematchPage } from './rematch';

@NgModule({
  declarations: [
    RematchPage,
  ],
  imports: [
    IonicPageModule.forChild(RematchPage),
  ],
})
export class RematchPageModule {}
