import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ChangePasswordPage } from '../change-password/change-password';
import { ContactUsPage } from '../contact-us/contact-us';
import { ChangeAccountNamesPage } from '../change-account-names/change-account-names';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  changePassword(){
    this.navCtrl.push(ChangePasswordPage)
  }
  contactUs(){
    this.navCtrl.push(ContactUsPage)
  }
  change(){
    this.navCtrl.push(ChangeAccountNamesPage)
  }
  delAccount(){
    const prompt = this.alertCtrl.create({
      title: 'Delete account',
      message: 'All your data will be lost. Are your sure to delete this account?',
        
      buttons: [
        {
          text: 'No',
          handler: data => {
            console.log('No clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            console.log('Yes clicked');
          }
        },
      ]
    });
    prompt.present();
  
  }
}
