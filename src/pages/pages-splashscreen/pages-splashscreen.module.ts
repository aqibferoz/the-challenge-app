import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagesSplashscreenPage } from './pages-splashscreen';

@NgModule({
  declarations: [
    PagesSplashscreenPage,
  ],
  imports: [
    IonicPageModule.forChild(PagesSplashscreenPage),
  ],
})
export class PagesSplashscreenPageModule {}
