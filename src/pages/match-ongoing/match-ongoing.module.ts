import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchOngoingPage } from './match-ongoing';

@NgModule({
  declarations: [
    MatchOngoingPage,
  ],
  imports: [
    IonicPageModule.forChild(MatchOngoingPage),
  ],
})
export class MatchOngoingPageModule {}
