import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MatchFinishedPage } from '../match-finished/match-finished';

/**
 * Generated class for the MatchOngoingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-match-ongoing',
  templateUrl: 'match-ongoing.html',
})
export class MatchOngoingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MatchOngoingPage');
  }
  finish(){
    const confirm = this.alertCtrl.create({
      title: 'Alert',
      message: "Are you sure?",
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('No clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.navCtrl.push(MatchFinishedPage);
          }
        }
      ]
    });
    confirm.present();
  }
}
