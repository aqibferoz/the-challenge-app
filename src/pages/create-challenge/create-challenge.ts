import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { map, finalize } from 'rxjs/operators';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from 'angularfire2/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HelperProvider } from '../../providers/helper/helper';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-create-challenge',
  templateUrl: 'create-challenge.html',
})
export class CreateChallengePage {

  amount: number;
  gameType: string;
  gameTypes: any;
  challengeName: string;
  imageUrl: string;

  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  createdCode = null;
  sourcex: any;
  base64Image: string;
  uploadImageId: any;

  constructor(public navCtrl: NavController, public navParams:
    NavParams, private api: ApiProvider, private camera: Camera,
    private afStorage: AngularFireStorage, private helper: HelperProvider) {

    this.api.getGameType().pipe(map(
      types => {
        return types.map(
          type => {
            return { id: type.payload.doc.id, ...type.payload.doc.data() }
          }
        )
      }
    )).subscribe(gameTypesRes => {
      this.gameTypes = gameTypesRes;
      console.log(this.gameTypes);
    });
  }

  createChallenge() {

    this.helper.load('Creating new challenge...');

    const challenge = {
      amount: this.amount,
      gameType: this.gameType,
      challengeName: this.challengeName,
      imageUrl: this.imageUrl
    }

    this.api.createChallenge(challenge).then(
      c => {
        this.navCtrl.push(HomePage);
        this.helper.dismiss();
      }
    ).catch(err => this.helper.presentBottomToast('Invalid data in one of the feild'));
  }

  uploadPhoto() {

    this.helper.load('Uploading image...');
    const id = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref(id);
    let task = this.ref.putString(this.base64Image, 'data_url');
    task.snapshotChanges()
      .pipe(finalize(() => {
        this.ref.getDownloadURL().subscribe(url => {
          this.helper.dismiss();
          this.helper.presentBottomToast('Image is uploaded successfully...');
          this.imageUrl = url;
        });
      })).subscribe();

  }

  takePhoto(source) {

    if (source === 'camera') {
      this.sourcex = this.camera.PictureSourceType.CAMERA;
    } else if (source === 'library') {
      this.sourcex = this.camera.PictureSourceType.PHOTOLIBRARY;
    }
    const options: CameraOptions = {
      sourceType: this.sourcex,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.uploadPhoto();
    }, (err) => {
      console.log(err);
    });

  }

  choosePicture() {
    let myfunc = () => {
      this.takePhoto('library');
    };
    let myfunc1 = () => {
      this.takePhoto('camera');
    };
    this.helper.presentActionSheet('Choose an option.', 'Gallery', 'Camera', myfunc, myfunc1);
  }

}
