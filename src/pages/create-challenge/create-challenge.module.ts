import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateChallengePage } from './create-challenge';

@NgModule({
  declarations: [
    CreateChallengePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateChallengePage),
  ],
})
export class CreateChallengePageModule {}
