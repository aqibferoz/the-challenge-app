import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddopponentPage } from '../addopponent/addopponent';

@IonicPage()
@Component({
  selector: 'page-challenge',
  templateUrl: 'challenge.html',
})
export class ChallengePage {

  private item: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = navParams.get('i');
  }

  addOpponent() {
    this.navCtrl.push(AddopponentPage)
  }

}
