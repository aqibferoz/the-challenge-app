import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  @Input() events: any;

  data = {
    "errorUsername":"Invalid username",
    "logo": "assets/images/logo/login.png",
    "errorUser": "Field can't be empty.",
    "errorPassword": "Field can't be empty."
  };

  public email: string;
  public password: string;
  confrimPassword: string;
  date: any;
  public username: string;

  private isEmailValid: boolean = true;
    private isUsernameValid: boolean = true;
    private isPasswordValid: boolean = true;
    private isCityValid: boolean = true;
    private isCountryValid: boolean = true;

    private regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private auth: AuthProvider, private api: ApiProvider,
    private helper: HelperProvider) {
  }

  async register() {

    try {
      
      this.helper.load('Signing up');

      if (this.email != null && this.password != null && this.confrimPassword != null &&
        this.data != null && this.username != null) {

        if (this.password === this.confrimPassword) {
          const user = await this.auth.signup(this.email, this.password);
          let u = {
            uid: user.user.uid,
            dateOfBirth: this.date,
            email: this.email,
            username: this.username
          }
          this.api.saveUser(user.user.uid, u).then(() => {
            this.helper.dismiss();
            this.navCtrl.push(LoginPage);
          }).catch(err => {
            this.helper.presentBottomToast(err);
            console.log(err);
          });
        } else {
          this.helper.dismiss();
          this.helper.presentBottomToast('Password dont match please enter same password');
        }

      } else {
        this.helper.presentBottomToast('Either please fill the complete form');
        this.helper.dismiss();
      }

    } catch (error) {
      this.helper.dismiss();
      this.helper.presentBottomToast(error);
      console.log(error);
    }

  }

  onEvent = (event: string): void => {
    if (event == "onRegister" && !this.validate()) {
        return;
    }
    if (this.events[event]) {
        this.events[event]({
            'username': this.username,
            'password': this.password,
            'email': this.email
        });
    }
}

  validate():boolean {
    this.isEmailValid = true;
    this.isUsernameValid = true;
    this.isPasswordValid = true;
    this.isCityValid = true;
    this.isCountryValid = true;

    if (!this.username ||this.username.length == 0) {
        this.isUsernameValid = false;
    }

    if (!this.password || this.password.length == 0) {
        this.isPasswordValid = false;
    }

    if (!this.password || this.password.length == 0) {
        this.isPasswordValid = false;
    }


    this.isEmailValid = this.regex.test(this.email);
    
    return this.isEmailValid && 
        this.isPasswordValid && 
        this.isUsernameValid && 
        this.isCityValid && 
        this.isCountryValid;
}
}
