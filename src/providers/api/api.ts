import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class ApiProvider {
  constructor(private firestore: AngularFirestore) { }

  saveUser(uid, data) {
    return this.firestore.collection('users').doc(uid).set(data);
  }

  getUser() {
    return this.firestore.collection('users').snapshotChanges();
  }

  getGameType() {
    return this.firestore.collection('gametype').snapshotChanges();
  }

  createChallenge(data) {
    return this.firestore.collection('challenges').add(data);
  }

  getChallenges() {
    return this.firestore.collection('challenges').snapshotChanges();
  }

}
