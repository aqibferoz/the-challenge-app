import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { AuthProvider } from '../providers/auth/auth';
import { SettingsPage } from '../pages/settings/settings';
import { LastMatchesPage } from '../pages/last-matches/last-matches';
import { CtPage } from '../pages/ct/ct';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private auth: AuthProvider) {
    platform.ready().then(() => {
      if (this.auth.getToken()) {
        this.rootPage = TabsPage;
      } else{
        this.rootPage = LoginPage;
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.show();
    });
  }
  settings(){
    this.nav.push(SettingsPage)
  }
  lastMatches(){
    this.nav.push(LastMatchesPage)
  }
  CT(){
    this.nav.push(CtPage)
  }
  logout(){
    this.auth.logout();
    this.nav.push(LoginPage);
  }
}
