import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, LoadingController } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';
import { ApiProvider } from '../providers/api/api';
import { LoginPage } from '../pages/login/login';

import { RegisterPage } from '../pages/register/register';
import { ChallengePage } from '../pages/challenge/challenge';
import { AddopponentPage } from '../pages/addopponent/addopponent';
import { StartgamePage } from '../pages/startgame/startgame';
import { GamepicturePage } from '../pages/gamepicture/gamepicture';
import { MatchOngoingPage } from '../pages/match-ongoing/match-ongoing';
import { MatchFinishedPage } from '../pages/match-finished/match-finished';
import { ChallengeFinishedPage } from '../pages/challenge-finished/challenge-finished';
import { RematchPage } from '../pages/rematch/rematch';
import { ResultNotClearPage } from '../pages/result-not-clear/result-not-clear';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { Camera } from '@ionic-native/camera';

import { config } from '../environment/environment';
import { HelperProvider } from '../providers/helper/helper';
import { AuthProvider } from '../providers/auth/auth';
import { CreateChallengePage } from '../pages/create-challenge/create-challenge';
import { SettingsPage } from '../pages/settings/settings';
import { ChangeAccountNamesPage } from '../pages/change-account-names/change-account-names';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { ContactUsPage } from '../pages/contact-us/contact-us';
import { LastMatchesPage } from '../pages/last-matches/last-matches';
import { CtPage } from '../pages/ct/ct';
import { DepositPage } from '../pages/deposit/deposit';
import { WithdrawPage } from '../pages/withdraw/withdraw';
import { AccountPage } from '../pages/account/account';
import { CategoriesPage } from '../pages/categories/categories';
import { ChosenCategoryPage } from '../pages/chosen-category/chosen-category';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    RegisterPage,
    ChallengePage,
    AddopponentPage,
    StartgamePage,
    GamepicturePage,
    MatchOngoingPage,
    MatchFinishedPage,
    ChallengeFinishedPage,
    RematchPage,
    ResultNotClearPage,
    CreateChallengePage,
    SettingsPage,
    ChangeAccountNamesPage,
    ChangePasswordPage,
    ContactUsPage,
    LastMatchesPage,
    CtPage,
    DepositPage,
    WithdrawPage,
    AccountPage,
    CategoriesPage,
    ChosenCategoryPage
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
    }, 
    )
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    RegisterPage,
    ChallengePage,
    AddopponentPage,
    StartgamePage,
    GamepicturePage,
    MatchOngoingPage,
    MatchFinishedPage,
    ChallengeFinishedPage,
    RematchPage,
    ResultNotClearPage,
    CreateChallengePage,
    SettingsPage,
    ChangeAccountNamesPage,
    ChangePasswordPage,
    ContactUsPage,
    LastMatchesPage,
    CtPage,
    DepositPage,
    WithdrawPage,
    AccountPage,
    CategoriesPage,
    ChosenCategoryPage
  ],
  providers: [
    StatusBar,
    Camera,
    SplashScreen,
    HttpClientModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    AuthProvider,
    HelperProvider
  ]
})
export class AppModule {}
